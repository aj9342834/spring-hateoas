/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.test.util;

import com.homemade.model.Order;
import org.junit.Assert;
/**
 *
 * @author Andrew
 */
public class OrderTestUtils {
   	public static void assertAllButIdsMatchBetweenOrders(Order expected, Order actual) {
    	Assert.assertEquals(expected.getDescription(), actual.getDescription());
    	Assert.assertEquals(expected.getCostInCents(), actual.getCostInCents());
    	Assert.assertEquals(expected.isComplete(), actual.isComplete());
    }
	
    public static Order generateTestOrder() {
    	Order order = new Order();
    	order.setDescription("test description");
    	order.setCostInCents(150L);
    	order.markIncomplete();
    	return order;
    }
    
     public static Order generateUpdatedOrder(Order original) {
    	Order updated = new Order();
    	updated.setDescription(original.getDescription() + " updated");
    	updated.setCostInCents(original.getCostInCents() + 100);
    	updated.markComplete();
    	return updated;
    } 
}
