/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.test.integration.controller.util;

import com.homemade.model.Order;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static com.homemade.test.integration.controller.util.ControllerTestUtils.*;

import org.springframework.hateoas.EntityLinks;
import org.springframework.test.web.servlet.ResultMatcher;

/**
 *
 * @author Andrew
 */
public class OrderControllerTestUtils {
    public static ResultMatcher orderAtIndexIsCorrect(int index, Order expected) {
		return new CompositeResultMatcher()
			.addMatcher(jsonPath("$.[" + index + "].id").value(expected.getId()))
			.addMatcher(jsonPath("$.[" + index + "].description").value(expected.getDescription()))
			.addMatcher(jsonPath("$.[" + index + "].costInCents").value(expected.getCostInCents()))
			.addMatcher(jsonPath("$.[" + index + "].complete").value(expected.isComplete()));
	}
	
	public static ResultMatcher orderIsCorrect(Order expected) {
		return orderIsCorrect(expected.getId(), expected);
	}
	
	private static ResultMatcher orderIsCorrect(Long expectedId, Order expected) {
		return new CompositeResultMatcher().addMatcher(jsonPath("$.id").value(expectedId))
			.addMatcher(jsonPath("$.description").value(expected.getDescription()))
			.addMatcher(jsonPath("$.costInCents").value(expected.getCostInCents()))
			.addMatcher(jsonPath("$.complete").value(expected.isComplete()));
	}
	
	public static ResultMatcher updatedOrderIsCorrect(Long originalId, Order expected) {
		return orderIsCorrect(originalId, expected);
	}
	
	public static ResultMatcher orderLinksAtIndexAreCorrect(int index, Order expected, EntityLinks entityLinks) {
		final String selfReference = entityLinks.linkForSingleResource(expected).toString();
		
		return new CompositeResultMatcher()
			.addMatcher(selfLinkAtIndexIs(index, selfReference))
			.addMatcher(updateLinkAtIndexIs(index, selfReference))
			.addMatcher(deleteLinkAtIndexIs(index, selfReference));
	}
	
	public static ResultMatcher orderLinksAreCorrect(Order expected, EntityLinks entityLinks) {
		final String selfReference = entityLinks.linkForSingleResource(expected).toString();
		
		return new CompositeResultMatcher()
			.addMatcher(selfLinkIs(selfReference))
			.addMatcher(updateLinkIs(selfReference))
			.addMatcher(deleteLinkIs(selfReference));
	}
}
