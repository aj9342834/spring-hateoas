/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.test.integration.controller.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

/**
 *
 * @author Andrew
 */
public class CompositeResultMatcher implements ResultMatcher {

	private List<ResultMatcher> matchers = new ArrayList<>();

	@Override
	public void match(MvcResult result) throws Exception {

		for (ResultMatcher matcher: matchers) {
			matcher.match(result);
		}
	}
	
	public CompositeResultMatcher addMatcher(ResultMatcher matcher) {
		matchers.add(matcher);
		return this;
	}
    
}
