/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.test.acceptance;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author Andrew
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/acceptance/features")
@WebAppConfiguration
public class OrderAcceptanceTests {
    
}
