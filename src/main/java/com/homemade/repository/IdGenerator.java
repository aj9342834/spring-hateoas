/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.repository;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Andrew
 */
//Scope prototype means that every time you ask spring (getBean or dependency injection) for an instance
//it will create a new instance and give a reference to that.
@Component // This annotation allows our Spring boot application to recognize (through component scanning) our generator class as an injectable component. 
// This will allow us to use the@Autowired annotation in other classes and have our component injected without having to create the generator using the new operator.
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IdGenerator {
    
    private AtomicLong nextId = new AtomicLong(1);
    
    public long getNextId() {
        return nextId.getAndIncrement();
    }
    
}
