/*
With our resource class in place, we need to implement an assembler that will create anOrderResource from an Order domain object. To do this, we will focus 
on two methods: (1)toResource, which consumes a single Order object and produces an OrderResource object, and (2) toResourceCollection, which consumes
a collection of Order objects and produces a collection of OrderResource objects. Since we can implement the latter in terms of the former, we will abstract
this relationship into an ABC
 */
package com.homemade.resource;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Andrew
 */

public abstract class ResourceAssembler<DomainType, ResourceType> {
    public abstract ResourceType toResource(DomainType domainObject);
    
    public Collection<ResourceType> toResourceCollection(Collection<DomainType> domainObjects) {
        return domainObjects.stream().map(o -> toResource(o)).collect(Collectors.toList());
    }
}
