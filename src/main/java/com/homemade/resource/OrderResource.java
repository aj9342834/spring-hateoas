/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.homemade.model.Order;
import org.springframework.hateoas.ResourceSupport;

/**
 *
 * @author Andrew
 */
public class OrderResource extends ResourceSupport {
    
    
    private final long id;
    private final String description;
    private final long costInCents;
    private final boolean isComplete;
    public OrderResource(Order order) {
        id = order.getId();
        description = order.getDescription();
        costInCents = order.getCostInCents();
        isComplete = order.isComplete();
    }
    
//    One final note on our OrderResource class: We cannot use the getId()method as our getter for our ID since the ResourceSupport class has a default
//    getId() method that returns a link. Therefore, we use the getResourceId() method as our getter for our id field; thus, we have to annotate our getResourceId()
//    method since, by default, our resource would serialize the ID field to resourceId due to the name of the getter method. To force this property to be
//    serialized to id, we use the @JsonProperty("id") annotation.
    @JsonProperty("id")  
    public Long getResourceId() {
        return id;
    }
    public String getDescription() {
        return description;
    }
    public long getCostInCents() {
        return costInCents;
    }
    public boolean isComplete() {
        return isComplete;
    }
}
