/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.resource;

import com.homemade.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

/**
 *
 * @author Andrew
 */
@Component
public class OrderResourceAssembler extends ResourceAssembler<Order, OrderResource> {
    @Autowired
    protected EntityLinks entityLinks;
    private static final String UPDATE_REL = "update";
    private static final String DELETE_REL = "delete";
    @Override
    public OrderResource toResource(Order order) {
        OrderResource resource = new OrderResource(order);
        //Using the EntityLinks class, we can create a link to our own resource by specifying (using the linkToSingleResource method) that we wish to create
        //a link to an Order , which uses the Spring HATEOAS Identifiable interface to obtain the ID of the object. 
        final Link selfLink = entityLinks.linkToSingleResource(order);
        
        //We then reuse this link to create three separate links: (1) a self link, (2) an update link, and (3) a delete link
        //The self link tells the consumer that if a link to this resource is needed, the provided HREF can be used.
        resource.add(selfLink.withSelfRel());
        //The update and delete links tell the consumer that if this resource should be updated or deleted, respectively, the provided HREF should be used.
        resource.add(selfLink.withRel(UPDATE_REL));
        resource.add(selfLink.withRel(DELETE_REL));
        //
        return resource;
    }
}
