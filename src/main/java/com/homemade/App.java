/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;


/**
 *
 * @author Andrew
 */
//@ComponentScan -  tells Spring to look for other components, configurations, and services in the the specified package. Spring is able to
//auto scan, detect and register your beans or components from pre-defined project package. If no package is specified current class package is taken as the root package.
//@EnableAutoConfiguration -  annotation tells Spring Boot to "guess" how you will want to configure Spring, based on the jar dependencies that you have added. 
//For example, If HSQLDB is on your classpath, and you have not manually configured any database connection beans, then Spring will auto-configure an in-memory database

// @SpringBootApplication  - allows you to use a single annotation rather than a few that are commonly used, like the two you mentioned above. 
// Actually the @SpringBootApplication annotation is equivalent to using @Configuration, @EnableAutoConfiguration and @ComponentScan with their default attributes.
@EnableEntityLinks// The @EnableEntityLinks annotation configures Spring to include support for the EntityLinks class in our system (allowing us to inject the EntityLinks object). 
@EnableHypermediaSupport(type = HypermediaType.HAL) // Likewise, the@EnableHypermediaSupport annotation instructs Spring to include support for HATEOAS, using the Hypermedia Application Language (HAL) when producing links.
@SpringBootApplication // marks our application a Spring Boot application, which configures the boilerplate code needed to start Spring and also instructs Spring to component scan our packages to find injectable classes (such as those annotated with @Component or @Repository).
public class App { 
 
    public static void main(String[] args){

        SpringApplication.run(App.class, args);
    }
    
    
}
