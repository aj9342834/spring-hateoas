/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.model;


/**
 *
 * @author Andrew
 */
public interface Identifiable  extends org.springframework.hateoas.Identifiable<Long> {
     public void setId(Long id);
}
