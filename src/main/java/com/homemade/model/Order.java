/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homemade.model;

/**
 *
 * @author Andrew
 */
public class Order implements Identifiable {

    private Long id;
    private String description;
    private long costInCents;
    private boolean isComplete;
    
    @Override
    public void setId(Long id) {
       this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }
    
     public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setCostInCents(long cost) {
        costInCents = cost;
    }
    public long getCostInCents() {
        return costInCents;
    }
    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }
    public void markComplete() {
        setComplete(true);
    }
    public void markIncomplete() {
        setComplete(false);
    }
    public boolean isComplete() {
        return isComplete;
    }
    
}
